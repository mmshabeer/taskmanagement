package com.iot.apps.tms.repo;

import com.iot.apps.tms.domain.Task;
import com.iot.apps.tms.domain.User;
import com.iot.apps.tms.repository.TaskRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
public class TaskRepositoryTest {

    @Autowired
    private TaskRepository taskRepository;

    @Test
    public void testCreateTask() {
        Task task = new Task("Test Task by Shabeer", "using spring data");
        User user = new User("Shabeer", "mohamedshabeerm@gmail.com", "1");
        task.setUser(user);
        taskRepository.save(task);
        System.out.println("Task Details " + task.toString());
        Assert.assertTrue(task.getId() > 0);
    }

}
