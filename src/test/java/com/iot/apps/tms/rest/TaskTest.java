package com.iot.apps.tms.rest;

import com.iot.apps.tms.TaskManagementApplicationTests;
import com.iot.apps.tms.domain.Task;
import com.iot.apps.tms.domainvalue.TaskPriorityEnum;
import com.iot.apps.tms.domainvalue.TaskStatusEnum;
import com.iot.apps.tms.service.TaskService;
import com.iot.apps.tms.service.UserService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TaskTest extends TaskManagementApplicationTests {

    @Autowired
    private TaskService taskService;

    @Autowired
    private UserService userService;

    @Test
    public void createTaskTest() throws Exception {
        Task task = new Task();
        task.setDescription("Test task creation through unit test");
        task.setTitle("Test Task");
        task.setUser(userService.getUserById(31l));
        task.setTaskStatus(taskService.getTaskStatus(TaskStatusEnum.NOT_STARTED));
        task.setTaskPriority(taskService.getTaskPriority(TaskPriorityEnum.LOW));

        this.getMockMvc().perform(post("/tasks")
                .content(getJsonObjectAsString(task))
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated());
    }

    @Test
    public void testGetAllTasksByPriority() throws Exception {
        this.getMockMvc().perform(get("/tasks/priority/LOW")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(jsonPath("$[0].id").isNotEmpty());
    }

    @Test
    public void testGetAllTasksByStatus() throws Exception {
        this.getMockMvc().perform(get("/tasks/status/IN_PROGRESS")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(jsonPath("$[0].id").isNotEmpty());
    }

    @Test
    public void testGetAllTasksByUser() throws Exception {
        this.getMockMvc().perform(get("/tasks/user/Balaji")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(jsonPath("$[0].id").isNotEmpty());
    }
}
