package com.iot.apps.tms.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.iot.apps.tms.TaskManagementApplicationTests;
import com.iot.apps.tms.domain.User;
import org.junit.Test;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserTest extends TaskManagementApplicationTests {

    @Test
    public void createUserTest() throws Exception {
        User user = new User();
        user.setEmail("testuser@tasktest.com");
        user.setName("Test user");
        user.setStatus("1");

        this.getMockMvc().perform(post("/user/create/")
                .content(getJsonObjectAsString(user))
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated());
    }
}
