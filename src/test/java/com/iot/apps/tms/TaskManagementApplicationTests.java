package com.iot.apps.tms;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest(
		classes = TaskManagementApplication.class)
@AutoConfigureMockMvc
public class TaskManagementApplicationTests {


	@Autowired
	private MockMvc mockMvc;

	@Test
	public void contextLoads() {
	}

	public static String getJsonObjectAsString(Object instance) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		String value = mapper.writeValueAsString(instance);
		System.out.println("Response is ::: " +  value);
		return value;
	}

	public MockMvc getMockMvc() {
		return this.mockMvc;
	}
}
