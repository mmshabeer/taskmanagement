package com.iot.apps.tms.service.impl;

import com.iot.apps.tms.domain.User;
import com.iot.apps.tms.repository.UserRepository;
import com.iot.apps.tms.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    public User createUser(User user) {
        return userRepository.save(user);
    }

    public User getUserById(Long userId) {
       return userRepository.findById(userId).get();
    }
}
