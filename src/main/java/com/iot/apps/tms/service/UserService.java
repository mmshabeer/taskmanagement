package com.iot.apps.tms.service;

import com.iot.apps.tms.domain.User;

public interface UserService {

    User createUser(User user);

    User getUserById(Long userId);
}
