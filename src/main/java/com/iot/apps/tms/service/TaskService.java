package com.iot.apps.tms.service;

import com.iot.apps.tms.domain.Task;
import com.iot.apps.tms.domain.TaskPriority;
import com.iot.apps.tms.domain.TaskStatus;
import com.iot.apps.tms.domainvalue.TaskPriorityEnum;
import com.iot.apps.tms.domainvalue.TaskStatusEnum;

import java.util.Collection;
import java.util.List;

public interface TaskService {

    Task createTask(Task task);

    List<Task> getAllTasks();

    TaskStatus getTaskStatus(TaskStatusEnum status);

    TaskPriority getTaskPriority(TaskPriorityEnum priority);

    Collection<Task> getTasksByPriority(TaskPriorityEnum priority);

    Collection<Task> getTasksByStatus(TaskStatusEnum status);

    Collection<Task> getTasksByUser(String assigned);
}
