package com.iot.apps.tms.service.impl;

import com.iot.apps.tms.domain.Task;
import com.iot.apps.tms.domain.TaskPriority;
import com.iot.apps.tms.domain.TaskStatus;
import com.iot.apps.tms.domainvalue.TaskPriorityEnum;
import com.iot.apps.tms.domainvalue.TaskStatusEnum;
import com.iot.apps.tms.repository.TaskPriorityRepository;
import com.iot.apps.tms.repository.TaskRepository;
import com.iot.apps.tms.repository.TaskStatusRepository;
import com.iot.apps.tms.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private TaskStatusRepository taskStatusRepository;

    @Autowired
    private TaskPriorityRepository taskPriorityRepository;

    @Override
    public Task createTask(Task task) {
        return taskRepository.save(task);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Task> getAllTasks() {
        List<Task> tasks = new ArrayList<>();
        taskRepository.findAll().forEach(task -> tasks.add(task));
        return tasks;
    }

    @Override
    public TaskStatus getTaskStatus(TaskStatusEnum status) {
        return taskStatusRepository.findByValue(status.name());
    }

    @Override
    public TaskPriority getTaskPriority(TaskPriorityEnum priority) {
        return taskPriorityRepository.findByValue(priority.name());
    }

    @Override
    public Collection<Task> getTasksByPriority(TaskPriorityEnum priority) {
        return taskRepository.getTasksByTaskPriority(priority.name());
    }

    @Override
    public Collection<Task> getTasksByStatus(TaskStatusEnum status) {
        return taskRepository.getTasksByTaskStatus(status.name());
    }

    @Override
    public Collection<Task> getTasksByUser(String assigned) {
        return taskRepository.getTasksByAssignees(assigned);
    }


}
