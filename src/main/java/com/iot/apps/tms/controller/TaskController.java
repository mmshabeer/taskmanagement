package com.iot.apps.tms.controller;

import com.iot.apps.tms.domain.Task;
import com.iot.apps.tms.domainvalue.TaskPriorityEnum;
import com.iot.apps.tms.domainvalue.TaskStatusEnum;
import com.iot.apps.tms.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collection;

@RestController
@RequestMapping("/tasks")
public class TaskController {

    @Autowired
    private TaskService taskService;

    @PostMapping("")
    public ResponseEntity<Task> addTask(@RequestBody Task task, UriComponentsBuilder builder) {
        HttpHeaders headers = new HttpHeaders();

        if (task.getId() == null) {
            task = taskService.createTask(task);
            headers.setLocation(builder.path("/task/{id}").buildAndExpand(task.getId()).toUri());
            return new ResponseEntity<>(task, HttpStatus.CREATED);
        }else {
            headers.set("msg", "Invalid Task");
            return new ResponseEntity<>(task, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @GetMapping("")
    public ResponseEntity<Collection<Task>> getAllTasks() {
        return new ResponseEntity<>(taskService.getAllTasks(), HttpStatus.OK);
    }

    @GetMapping("/priority/{priority}")
    public ResponseEntity<Collection<Task>> getAllTasksByPriority(@PathVariable String priority) {
        return new ResponseEntity<>(taskService.getTasksByPriority(TaskPriorityEnum.valueOf(priority)), HttpStatus.OK);
    }

    @GetMapping("/status/{status}")
    public ResponseEntity<Collection<Task>> getAllTasksByStatus(@PathVariable String status) {
        return new ResponseEntity<>(taskService.getTasksByStatus(TaskStatusEnum.valueOf(status)), HttpStatus.OK);
    }

    @GetMapping("/user/{name}")
    public ResponseEntity<Collection<Task>> getAllTasksByUser(@PathVariable String name) {
        return new ResponseEntity<>(taskService.getTasksByUser(name), HttpStatus.OK);
    }
}
