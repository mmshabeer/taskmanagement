package com.iot.apps.tms.repository;

import com.iot.apps.tms.domain.TaskStatus;
import org.springframework.data.neo4j.repository.Neo4jRepository;

public interface TaskStatusRepository extends Neo4jRepository<TaskStatus, Long> {

    TaskStatus findByValue(String value);
}
