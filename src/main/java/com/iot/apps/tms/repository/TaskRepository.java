package com.iot.apps.tms.repository;

import com.iot.apps.tms.domain.Task;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;

public interface TaskRepository extends Neo4jRepository<Task, Long> {

    @Query("MATCH (t:Task)-[r:PRIORITY]->(p:Priority) where p.value = {taskPriority} RETURN t,r,p as taskPriority")
    Collection<Task> getTasksByTaskPriority(@Param("taskPriority") String taskPriority);

    @Query("MATCH (t:Task)-[r:STATUS]->(s:Status) where s.value = {taskStatus} RETURN t,r,s as taskStatus")
    Collection<Task> getTasksByTaskStatus(@Param("taskStatus") String taskStatus);

    @Query("MATCH (t:Task)-[r:ASSIGNED_TO]->(u:User) where u.name contains  {assigned} return t,r,u")
    Collection<Task> getTasksByAssignees(@Param("assigned") String assigned);
}