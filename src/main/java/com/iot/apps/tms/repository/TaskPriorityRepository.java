package com.iot.apps.tms.repository;

import com.iot.apps.tms.domain.TaskPriority;
import org.springframework.data.neo4j.repository.Neo4jRepository;

public interface TaskPriorityRepository  extends Neo4jRepository<TaskPriority, Long> {

    TaskPriority findByValue(String value);
}
