package com.iot.apps.tms.domainvalue;

public enum TaskPriorityEnum {
    LOW(1), MEDIUM(2), HIGH(3);

    int id;

    TaskPriorityEnum(int id) {
        this.id = id;
    }
}
