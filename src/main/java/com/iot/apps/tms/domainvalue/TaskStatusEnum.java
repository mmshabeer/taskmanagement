package com.iot.apps.tms.domainvalue;

public enum TaskStatusEnum {
    DRAFT(1), NOT_STARTED(2), IN_PROGRESS(3), ON_HOLD(4), COMPLETED(5), CANCELLED(6);

    int id;
    TaskStatusEnum(int id) {
        this.id = id;
    }
}
